import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class AddUserGuard extends AuthGuard('jwt') {
	// https://stackoverflow.com/questions/53426069/getting-user-data-by-using-guards-roles-jwt
	handleRequest(err, user, info: Error) {
		return user;
	}
}
