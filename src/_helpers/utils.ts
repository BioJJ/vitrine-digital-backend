import { Like } from 'typeorm';

export function removePagination(query: Object) {
	delete query['page'];
	delete query['limit'];

	return query;
}

export function _Like(prop: string | number) {
	return Like(`%${!!prop ? prop : ''}%`);
}
