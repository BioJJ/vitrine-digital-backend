import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Acesso } from 'src/entities/acesso.entity';
import { ProdutoModule } from 'src/produto/produto.module';
import { AcessoController } from './controller/acesso.controller';
import { AcessoService } from './service/acesso.service';

@Module({
  imports:[TypeOrmModule.forFeature([Acesso]), forwardRef(() => ProdutoModule)],
  controllers: [AcessoController],
  providers: [AcessoService],
  exports: [AcessoService]
})
export class AcessoModule {}
