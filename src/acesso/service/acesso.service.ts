import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Acesso } from 'src/entities/acesso.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AcessoService {
    constructor(@InjectRepository(Acesso) private repository: Repository<Acesso>){}

    save(input: Partial<Acesso>) {
        return this.repository.save(input);
    }
}
