import { Module } from '@nestjs/common';
import { AuthController } from './controller/auth.controller';
import { AuthService } from './service/auth.service';
import { UsuarioModule } from 'src/usuario/usuario.module';
import { JwtStrategy } from './jwt.service';
import { PassportModule } from '@nestjs/passport';

@Module({
	imports: [
		UsuarioModule,
		PassportModule,
		PassportModule.register({ defaultStrategy: 'jwt' }),
	],
	controllers: [AuthController],
	providers: [AuthService, JwtStrategy],
})
export class AuthModule {}
