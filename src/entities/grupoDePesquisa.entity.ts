import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { SimpleEntity } from './simpleEntity.entity';
import { LinhaDePesquisa } from './linhaDePesquisa.entity';
import { Usuario } from './usuario.entity';
import { Produto } from './produto.entity';

@Entity()
export class GrupoDePesquisa extends SimpleEntity {
	@Column()
	sigla: string;

	@ManyToOne(
		type => LinhaDePesquisa,
		linhaDePesquisa => linhaDePesquisa.gruposDePesquisa,
		{ nullable: false, eager: true },
	)
	linhaDePesquisa: LinhaDePesquisa;

	@OneToMany(
		type => Usuario,
		usuario => usuario.grupoDePesquisa,
		{ cascade: true },
	)
	usuarios: Usuario[];

	@OneToMany(
		type => Produto,
		produto => produto.grupoDePesquisa,
	)
	produtos: Produto[];
}
