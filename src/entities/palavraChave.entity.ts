import { Entity, Column } from "typeorm";
import { AbstractEntity } from "./abstractEntity.entity";

@Entity()
export class PalavraChave extends AbstractEntity {
    @Column()
    nome: string;
}