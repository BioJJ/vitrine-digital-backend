import {
	Column,
	Entity,
	ManyToOne,
	OneToMany,
	JoinTable,
	ManyToMany,
} from 'typeorm';
import { GrupoDePesquisa } from './grupoDePesquisa.entity';
import { Usuario } from './usuario.entity';
import { FichaTecnica } from './fichaTecnica';
import { AbstractEntity } from './abstractEntity.entity';
import { ProdutoImagem } from './produtoImagem.entity';
import { PalavraChave } from './palavraChave.entity';
import { Acesso } from './acesso.entity';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity()
export class Produto extends AbstractEntity {
	@Field()
	@Column()
	titulo: string;

	@Field({ nullable: true })
	@Column({ nullable: true })
	site: string;

	@Field(type => Object, { nullable: true })
	@Column('json', { nullable: true })
	fotos: Object;

	@Column('json', { nullable: true })
	paragrafos;

	@ManyToMany(type => PalavraChave, { eager: true, cascade: true })
	@JoinTable()
	palavrasChave: PalavraChave[];

	@Column('json', { nullable: true })
	videos;

	@ManyToOne(
		type => GrupoDePesquisa,
		grupoDePesquisa => grupoDePesquisa.produtos,
		{ nullable: false, eager: true },
	)
	grupoDePesquisa: GrupoDePesquisa;

	@ManyToOne(type => Usuario, { nullable: false, eager: true })
	proprietario: Usuario;

	@Column(type => FichaTecnica)
	fichaTecnica: FichaTecnica;

	@OneToMany(
		type => ProdutoImagem,
		produtoImagem => produtoImagem.produto,
		{ eager: true },
	)
	imagens: ProdutoImagem[];

	@OneToMany(
		type => Acesso,
		acesso => acesso.produto,
	)
	acessos: ProdutoImagem[];

	/* Possíveis campos calculados */
	@Field()
	@Column({ insert: false, select: false, nullable: true })
	totalAcessos: number;
}
