import { Entity, Column, ManyToOne } from 'typeorm';
import { AbstractEntity } from './abstractEntity.entity';
import { Produto } from './produto.entity';

@Entity()
export class ProdutoImagem extends AbstractEntity {
	@Column()
    imagem: string;
    
    @Column() 
    nome: string;

	@ManyToOne(
		type => Produto,
		produto => produto.imagens,
		{ nullable: false },
	)
	produto: Produto;
}
