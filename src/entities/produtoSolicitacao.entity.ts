import { Entity, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { AbstractEntity } from "./abstractEntity.entity";
import { Aprovador } from "./aprovador.entity";
import { Produto } from "./produto.entity";

@Entity()
export class ProdutoSolicitacao extends AbstractEntity {

    @OneToOne(() => Produto)
    @JoinColumn()
    produto: Produto;

    @OneToMany(
		type => Aprovador,
        aprovador => aprovador.solicitacao,
        {cascade: true, eager: true}
	)
    aprovadores: Aprovador[]
}