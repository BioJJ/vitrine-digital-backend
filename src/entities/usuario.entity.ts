import { Entity, ManyToOne, Column, OneToMany } from 'typeorm';
import { AbstractEntity } from './abstractEntity.entity';
import { GrupoDePesquisa } from './grupoDePesquisa.entity';
import { Exclude } from 'class-transformer';
import { ApiHideProperty } from '@nestjs/swagger';

@Entity()
export class Usuario extends AbstractEntity {
	@Column()
	nome: string;

	@Column()
	sobrenome: string;

	@Column({unique: true})
	email: string;

	@Exclude()
	@ApiHideProperty()
	@Column()
	senha: string;

	@Column({ nullable: true })
	citacao: string;

	@Column({ nullable: true })
	lattes: string;

	@Column({ nullable: true })
	descricao: string;

	@Column()
	cpf: string;

	@Column({ nullable: true })
	matricula: string;

	@ManyToOne(
		type => GrupoDePesquisa,
		grupoDePesquisa => grupoDePesquisa.usuarios,
		{ eager: true },
	)
	grupoDePesquisa: GrupoDePesquisa;

	@ManyToOne(
		type => Usuario,
		orientador => orientador.orientandos,
		{ nullable: true },
	)
	orientador: Usuario;

	@OneToMany(
		type => Usuario,
		usuario => usuario.orientador,
	)
	orientandos: Usuario[];

	@ManyToOne(
		type => Usuario,
		coorientador => coorientador.coorientandos,
		{ nullable: true },
	)
	coorientador: Usuario;

	@OneToMany(
		type => Usuario,
		usuario => usuario.coorientador,
	)
	coorientandos: Usuario[];

	@Column({default: ''})
	foto: string;

	@Column({default: false})
	isDocente: boolean;
	
	@Column({default: true})
	isDiscente: boolean;

	@Column({default: false})
	isExterno: boolean;

	@Column({ nullable: true })
	instituicao: string;
}
