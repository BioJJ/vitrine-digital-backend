import { Injectable } from '@nestjs/common';
import {
	MulterModuleOptions,
	MulterOptionsFactory,
} from '@nestjs/platform-express';
import * as GridFsStorage from 'multer-gridfs-storage';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';

@Injectable()
export class GridFsMulterConfigService implements MulterOptionsFactory {
	gridFsStorage: GridFsStorage;
	constructor(@InjectConnection('files') private readonly connection: Connection) {
		this.gridFsStorage = new GridFsStorage({
			url: process.env.MONGO_INITDB_ROOT_USERNAME && process.env.MONGO_INITDB_ROOT_PASSWORD ?
				 `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@vitrine-digital-storage:27017/vitrine`
				: 'mongodb://localhost:27017/vitrine',
			db: connection,
			file: (req, file) => {
				return new Promise((resolve, reject) => {
					const filename = file.originalname.trim();
					const fileInfo = {
						filename: filename,
					};
					resolve(fileInfo);
				});
			},
		});
	}

	createMulterOptions(): MulterModuleOptions {
		return {
			storage: this.gridFsStorage,
		};
	}
}
