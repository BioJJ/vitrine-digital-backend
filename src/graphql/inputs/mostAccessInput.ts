import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class MostAccessInput {
    @Field({nullable: true})
    minDate: Date;
    @Field({nullable: true})
    maxDate: Date;
    @Field()
    limit: number = 0;
    @Field()
    page: number = 1;
}