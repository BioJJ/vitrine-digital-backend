import { Test, TestingModule } from '@nestjs/testing';
import { GrupoDePesquisaController } from './grupo-de-pesquisa.controller';

describe('GrupoDePesquisa Controller', () => {
  let controller: GrupoDePesquisaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GrupoDePesquisaController],
    }).compile();

    controller = module.get<GrupoDePesquisaController>(GrupoDePesquisaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
