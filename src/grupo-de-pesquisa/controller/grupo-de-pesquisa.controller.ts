import { Controller, Get, Post, Body, UseInterceptors, ClassSerializerInterceptor, Query } from '@nestjs/common';
import { ApiOperation, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { GrupoDePesquisa } from 'src/entities/grupoDePesquisa.entity';
import { GrupoDePesquisaService } from '../service/grupo-de-pesquisa.service';
import { GrupoDePesquisaDtoInput } from 'src/model/dto/dtoInput/grupoDePesquisaDtoInput.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { QueryOptions } from 'src/model/queryOptions.dto';
import { SearchGeneric } from 'src/model/dto/search/searchGeneric.dto';
import { removePagination } from 'src/_helpers/utils';

@ApiTags('Grupo de Pesquisa')
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
@Controller('grupos-de-pesquisa')
export class GrupoDePesquisaController {
	constructor(private service: GrupoDePesquisaService) {}

	@Get()
    @ApiOperation({description: 'Retorna uma listagem de itens'})
	getAll(@Query() options: QueryOptions, @Query() search: SearchGeneric): Promise<Pagination<GrupoDePesquisa>> {
		return this.service.getAll({...options, route: 'teste' }, removePagination(search)); // TODO: Trocar pela string de rota certa.
    }

	@Post()
	@ApiOperation({ description: 'Salva um registro' })
	saveOne(@Body() input: GrupoDePesquisaDtoInput): Promise<GrupoDePesquisa> {
		return this.service.saveOne(input);
	}
}
