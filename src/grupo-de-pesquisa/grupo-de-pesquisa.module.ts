import { Module } from '@nestjs/common';
import { GrupoDePesquisaController } from './controller/grupo-de-pesquisa.controller';
import { GrupoDePesquisaService } from './service/grupo-de-pesquisa.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GrupoDePesquisa } from 'src/entities/grupoDePesquisa.entity';

@Module({
  imports: [TypeOrmModule.forFeature([GrupoDePesquisa])],
  controllers: [GrupoDePesquisaController],
  providers: [GrupoDePesquisaService]
})
export class GrupoDePesquisaModule {}
