import { Test, TestingModule } from '@nestjs/testing';
import { GrupoDePesquisaService } from './grupo-de-pesquisa.service';

describe('GrupoDePesquisaService', () => {
  let service: GrupoDePesquisaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GrupoDePesquisaService],
    }).compile();

    service = module.get<GrupoDePesquisaService>(GrupoDePesquisaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
