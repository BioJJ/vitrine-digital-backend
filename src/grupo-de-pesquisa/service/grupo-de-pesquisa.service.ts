import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GrupoDePesquisa } from 'src/entities/grupoDePesquisa.entity';
import { Repository } from 'typeorm';
import { GrupoDePesquisaDtoInput } from 'src/model/dto/dtoInput/grupoDePesquisaDtoInput.dto';
import { Pagination, paginate, IPaginationOptions } from 'nestjs-typeorm-paginate';
import { _Like } from 'src/_helpers/utils';

@Injectable()
export class GrupoDePesquisaService {
  constructor(
    @InjectRepository(GrupoDePesquisa)
    private repository: Repository<GrupoDePesquisa>,
  ) {}

  async getAll(
		options: IPaginationOptions,
		search: Partial<GrupoDePesquisaDtoInput>,
	): Promise<Pagination<GrupoDePesquisa>> {
		const queryBuilder = this.repository.createQueryBuilder('g')
			.leftJoinAndSelect('g.linhaDePesquisa', 'linhaDePesquisa');
		if (Object.entries(search).length !== 0) {
			queryBuilder.where({
				...search,
				nome: _Like(search.nome),
				descricao: _Like(search.descricao),
			});
		}
		return paginate<GrupoDePesquisa>(queryBuilder, options);
	}

  async saveOne(input: GrupoDePesquisaDtoInput): Promise<GrupoDePesquisa> {
    return this.repository.save(input);
  }
}
