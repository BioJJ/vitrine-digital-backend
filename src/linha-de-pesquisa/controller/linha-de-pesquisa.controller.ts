import { Controller, Get, Post, Body, ClassSerializerInterceptor, UseInterceptors, Query, UseGuards, Put, Param, Request } from '@nestjs/common';
import { LinhaDePesquisaService } from '../service/linha-de-pesquisa.service';
import { LinhaDePesquisa } from '../../entities/linhaDePesquisa.entity';
import { ApiTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { Pagination } from 'nestjs-typeorm-paginate';
import { QueryOptions } from '../../model/queryOptions.dto';
import { Roles } from 'src/auth/role.decorator';
import { RolesGuard } from 'src/auth/role.guard';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SearchGeneric } from 'src/model/dto/search/searchGeneric.dto';
import { removePagination } from 'src/_helpers/utils'
import { SimpleEntityDTO } from 'src/model/dto/simpleEntityDTO.dto';

@ApiTags('Linha de Pesquisa')
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
@Controller('linhas-de-pesquisa')
export class LinhaDePesquisaController {
	constructor(private service: LinhaDePesquisaService) {}

    @Get()
    @ApiOperation({description: 'Retorna uma listagem de itens'})
	getAll(@Query() options: QueryOptions, @Query() search: SearchGeneric): Promise<Pagination<LinhaDePesquisa>> {
		return this.service.getAll({...options, route: 'teste' }, removePagination(search)); // TODO: Trocar pela string de rota certa.
    }

    @Post()
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('admin')
    @ApiOperation({description: 'Salva um registro'})
    saveOne(@Body() input: SimpleEntityDTO): Promise<LinhaDePesquisa> {
        return this.service.saveOne(input);
    }

    @Put(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('admin')
    @ApiOperation({description: 'Altera um registro'})
    updateOne(@Param('id') id: number, @Body() input: SimpleEntityDTO) {
        return this.service.update(id, input);
    }
    
}