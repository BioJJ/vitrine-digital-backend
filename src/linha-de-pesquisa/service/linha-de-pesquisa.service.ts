import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LinhaDePesquisa } from '../../entities/linhaDePesquisa.entity';
import { Repository } from 'typeorm';
import {
	paginate,
	Pagination,
	IPaginationOptions,
} from 'nestjs-typeorm-paginate';
import { _Like } from 'src/_helpers/utils';
import { SimpleEntityDTO } from 'src/model/dto/simpleEntityDTO.dto';

@Injectable()
export class LinhaDePesquisaService {
	constructor(
		@InjectRepository(LinhaDePesquisa)
		private repo: Repository<LinhaDePesquisa>,
	) {}

	async getAll(options: IPaginationOptions, search: Partial<SimpleEntityDTO>): Promise<Pagination<LinhaDePesquisa>> {
		const queryBuilder = this.repo.createQueryBuilder('l').orderBy({'l.nome': 'ASC'}).leftJoinAndSelect('l.gruposDePesquisa', 'grupoDePesquisa')
		
		if(Object.entries(search).length !== 0){
			queryBuilder.where(
				{
					...search,
					nome: _Like(search.nome),
					descricao: _Like(search.descricao)
				},
			)
		}
		return paginate<LinhaDePesquisa>(queryBuilder, options);
	}

	async saveOne(input: SimpleEntityDTO): Promise<LinhaDePesquisa> {
		return this.repo.save(input);
	}

	async update(id: number, input: SimpleEntityDTO){
		return this.repo.update(id, input);
	}
}
