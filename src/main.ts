import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import dotenvFlow = require('dotenv-flow');

async function bootstrap() {
  // https://medium.com/@helder.bertoldo/nestjs-como-utilizar-m%C3%BAltiplos-env-banco-de-dados-d96045852a24
  dotenvFlow.config();
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('Vitrine Digital')
    .setDescription('Endpoints disponíveis na API da Vitrine Digital')
    .setVersion('1.0')
    .addBearerAuth(
      {name: 'Authorization', type: 'apiKey', in: 'header'}
    )
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('swagger', app, document);

  await app.listen(3000);
}
bootstrap();
