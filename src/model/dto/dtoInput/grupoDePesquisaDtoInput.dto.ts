import { LinhaDePesquisa } from "../../../entities/linhaDePesquisa.entity";
import { SimpleEntityDTO } from "../simpleEntityDTO.dto";

export class GrupoDePesquisaDtoInput extends SimpleEntityDTO {
    sigla: string;
    linhaDePesquisa: LinhaDePesquisa
}