import { ApiProperty } from '@nestjs/swagger'
import { Usuario } from 'src/entities/usuario.entity';

export class UsuarioDtoInput {

    data: Usuario;
    
    @ApiProperty({type:"file"})
    file: any;
}