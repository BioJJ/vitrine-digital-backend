import { UsuarioLoginDtoOutput } from "./usuarioLoginDtoOutput.dto";

export class UsuarioDtoOutput {
    nome: string;
    sobrenome: string;
    email: string;
    citacao: string;
    lattes: string;
    descricao: string;
    orientador: UsuarioDtoOutput;
    coorientador: UsuarioDtoOutput; // TODO: Verificar DTO de usuário.
}