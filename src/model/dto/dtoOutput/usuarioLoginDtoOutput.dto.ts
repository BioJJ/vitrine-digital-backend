import { UsuarioDtoOutput } from "./usuarioDtoOutput.dto";

export class UsuarioLoginDtoOutput {
    usuario: UsuarioDtoOutput;
    token: string;
}