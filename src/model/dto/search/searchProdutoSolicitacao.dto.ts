enum STATUS {
    ATIVO = "ativo",
    INATIVO = "inativo"
}

export class SearchProdutoSolicitacao {
    status: STATUS;
}