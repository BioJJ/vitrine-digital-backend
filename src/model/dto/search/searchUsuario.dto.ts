export class SearchUsuario {
    nome?: string;
    email?: string;
    matricula?: string;
    docente?: boolean;
    discente?: boolean;
    externo?: boolean;
}