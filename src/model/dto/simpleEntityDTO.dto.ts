export class SimpleEntityDTO {
    id?: number;
    nome: string;
    descricao: string;
    ativo: boolean
}