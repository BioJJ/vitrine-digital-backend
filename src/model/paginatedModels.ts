import { ObjectType } from "@nestjs/graphql";
import { LinhaDePesquisa } from "src/entities/linhaDePesquisa.entity";
import { Produto } from "src/entities/produto.entity";
import { Paginated } from "./pagination";

@ObjectType()
export class PaginatedLinhaDePesquisa extends Paginated(LinhaDePesquisa) {}

@ObjectType()
export class PaginatedProduto extends Paginated(Produto) {}
