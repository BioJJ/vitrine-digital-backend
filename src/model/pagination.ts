import { Field, ObjectType, Int } from '@nestjs/graphql';
import { Type } from '@nestjs/common';

// https://docs.nestjs.com/graphql/resolvers
export function Paginated<T>(classRef: Type<T>): any {
	@ObjectType(`${classRef.name}Edge`)
	class PaginationMeta {
		/**
		 * the amount of items on this specific page
		 */
        @Field(type => Int)
		itemCount: number;
		/**
		 * the total amount of items
		 */
        @Field(type => Int)
		totalItems: number;
		/**
		 * the amount of items that were requested per page
		 */
        @Field(type => Int)
		itemsPerPage: number;
		/**
		 * the total amount of pages in this paginator
		 */
        @Field(type => Int)
		totalPages: number;
		/**
		 * the current page this paginator "points" to
		 */
        @Field(type => Int)
		currentPage: number;
	}

	@ObjectType({ isAbstract: true })
	abstract class PaginatedType {
		@Field(type => [classRef], { nullable: true })
		items: T[];

		@Field()
		meta: PaginationMeta;
	}
	return PaginatedType;
}
