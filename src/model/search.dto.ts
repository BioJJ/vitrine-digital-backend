export class Sort {
	sort: string;
	order?: Order = Order.ASC;
}

enum Order {
	ASC = 'ASC',
	DESC = 'DESC'
}