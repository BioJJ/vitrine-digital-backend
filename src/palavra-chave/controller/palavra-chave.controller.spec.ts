import { Test, TestingModule } from '@nestjs/testing';
import { PalavraChaveController } from './palavra-chave.controller';

describe('PalavraChave Controller', () => {
  let controller: PalavraChaveController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PalavraChaveController],
    }).compile();

    controller = module.get<PalavraChaveController>(PalavraChaveController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
