import { Injectable } from '@nestjs/common';
import { PalavraChave } from 'src/entities/palavraChave.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PalavraChaveService {
    constructor(@InjectRepository(PalavraChave) private repository: Repository<PalavraChave>){}

    findOne(id: number){
        return this.repository.findOneOrFail(id);
    }


    findOneByNome(nome: string) {
        return this.repository.findOne({where: {nome}});
    }

    save(input: PalavraChave) {
        return this.repository.save(input);
    }
}
