import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Pagination } from 'nestjs-typeorm-paginate';
import { ProdutoSolicitacao } from 'src/entities/produtoSolicitacao.entity';
import { SearchProdutoSolicitacao } from 'src/model/dto/search/searchProdutoSolicitacao.dto';
import { QueryOptions } from 'src/model/queryOptions.dto';
import { removePagination } from 'src/_helpers/utils';
import { ProdutoSolicitacaoService } from '../service/produto-solicitacao.service';

@ApiTags('Solicitações de Produtos')
@ApiBearerAuth()
@Controller('produto-solicitacoes')
export class ProdutoSolicitacaoController {
    constructor(private service: ProdutoSolicitacaoService) {}

    @Get()
    @ApiOperation({description: 'Retorna uma listagem de itens'})
	getAll(@Query() options: QueryOptions, @Query() search: SearchProdutoSolicitacao): Promise<Pagination<ProdutoSolicitacao>> {
		return this.service.getAll({...options, route: 'teste' }, removePagination(search)); // TODO: Trocar pela string de rota certa.
    }

    @Post()
    @ApiOperation({ description: 'Salva um registro' })
    async saveOne(@Body() input: ProdutoSolicitacao): Promise<ProdutoSolicitacao> {
        // TODO: Realizar validações
		return this.service.save(input);
	}
}
