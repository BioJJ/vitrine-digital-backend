import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import { ProdutoSolicitacao } from 'src/entities/produtoSolicitacao.entity';
import { SearchProdutoSolicitacao } from 'src/model/dto/search/searchProdutoSolicitacao.dto';
import { Repository } from 'typeorm';

@Injectable()
export class ProdutoSolicitacaoService {
	constructor(
		@InjectRepository(ProdutoSolicitacao)
		private repository: Repository<ProdutoSolicitacao>,
    ) {}
    
    save(input: Partial<ProdutoSolicitacao>) {
        return this.repository.save(input);
    }

    async getAll(options: IPaginationOptions, search: Partial<SearchProdutoSolicitacao>): Promise<Pagination<ProdutoSolicitacao>> {
        const queryBuilder = this.repository.createQueryBuilder("solicitacao")
        .leftJoinAndSelect("solicitacao.aprovadores", "aprovador")
        .leftJoinAndSelect("solicitacao.produto", "produto")
		.leftJoinAndSelect("produto.grupoDePesquisa", "grupoDePesquisa")
		.leftJoinAndSelect("grupoDePesquisa.linhaDePesquisa", "linhaDePesquisa")
		.leftJoinAndSelect("produto.imagens", "produtoImagem")
		.leftJoinAndSelect("produto.palavrasChave", "palavrasChave")
		.leftJoinAndSelect("produto.proprietario", "proprietario");
		if(Object.entries(search).length !== 0){
            const status = search.status == "ativo"
			search.status && queryBuilder.where("solicitacao.ativo = :status", {status})
		}
		return paginate<ProdutoSolicitacao>(queryBuilder, options);
	}
}
