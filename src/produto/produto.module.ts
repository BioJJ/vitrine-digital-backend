import { forwardRef, Module } from '@nestjs/common';
import { ProdutoService } from './service/produto.service';
import { ProdutoController } from './controller/produto.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Produto } from 'src/entities/produto.entity';
import { ProdutoImagem } from 'src/entities/produtoImagem.entity';
import { MulterModule } from '@nestjs/platform-express';
import { GridFsMulterConfigService } from 'src/files/GridFsConfiguration.service';
import { PalavraChaveModule } from 'src/palavra-chave/palavra-chave.module';
import { ObjectScalar } from 'src/relatorio/graphql/ObjectScalar';
import { AcessoModule } from 'src/acesso/acesso.module';
import { Acesso } from 'src/entities/acesso.entity';

@Module({
	imports: [
		TypeOrmModule.forFeature([Produto, ProdutoImagem, Acesso]),
		MulterModule.registerAsync({
			useClass: GridFsMulterConfigService,
		}),
		PalavraChaveModule,
	],
	providers: [ProdutoService, ObjectScalar],
	controllers: [ProdutoController],
	exports: [ProdutoService]
})
export class ProdutoModule {}
