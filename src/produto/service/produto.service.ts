import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Produto } from 'src/entities/produto.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import {
	IPaginationOptions,
	Pagination,
	paginate,
} from 'nestjs-typeorm-paginate';
import { _Like } from 'src/_helpers/utils';
import { ProdutoImagem } from 'src/entities/produtoImagem.entity';
import { SearchProduto } from 'src/model/dto/search/searchProduto.dto';
import { MostAccessInput } from 'src/graphql/inputs/mostAccessInput';

@Injectable()
export class ProdutoService {
	constructor(
		@InjectRepository(Produto) private repository: Repository<Produto>,
		@InjectRepository(ProdutoImagem)
		private repositoryImagem: Repository<ProdutoImagem>,
	) {}

	async getAll(
		options: IPaginationOptions,
		search: Partial<SearchProduto>,
	): Promise<Pagination<Produto>> {
		const queryBuilder = this.repository
			.createQueryBuilder('produto')
			.leftJoinAndSelect('produto.grupoDePesquisa', 'grupoDePesquisa')
			.leftJoinAndSelect(
				'grupoDePesquisa.linhaDePesquisa',
				'linhaDePesquisa',
			)
			.leftJoinAndSelect('produto.imagens', 'produtoImagem')
			.leftJoinAndSelect('produto.palavrasChave', 'palavrasChave')
			.leftJoinAndSelect('produto.proprietario', 'proprietario')
			.leftJoinAndSelect('proprietario.orientador', 'orientador')
			.leftJoinAndSelect('proprietario.coorientador', 'coorientador');
		if (Object.entries(search).length !== 0) {
			search.titulo &&
				queryBuilder.andWhere('titulo LIKE :titulo', {
					titulo: _Like(search.titulo.toLowerCase()).value,
				});

			search.proprietario &&
				queryBuilder.andWhere('proprietario.nome LIKE :nome', {
					nome: _Like(search.proprietario).value,
				});

			search.solicitanteId &&
				queryBuilder.andWhere(
					'(coorientador.id = :idSolicitante OR orientador.id = :idSolicitante OR proprietarioId = :idSolicitante)',
					{ idSolicitante: search.solicitanteId },
				);
		}
		return paginate<Produto>(queryBuilder, options);
	}

	async getMostAccess(mostAccessData: MostAccessInput) {
		const minDate = mostAccessData.minDate; // utcToZonedTime(mostAccessData.minDate, 'Europe/London');
		const maxDate = mostAccessData.maxDate; // utcToZonedTime(mostAccessData.maxDate, 'Europe/London');

		const limit = mostAccessData.limit;
		const page = mostAccessData.page;

		const subQuery = (subSelect: SelectQueryBuilder<any>) =>
			subSelect
				.select('produtoId')
				.addSelect('count(produtoId) as produto_totalAcessos')
				.from('acesso', 'acesso')
				.where(`acesso.createdAt BETWEEN :minDate AND :maxDate`, {
					minDate,
					maxDate,
				})
				.groupBy('produtoId');

		const queryBuilder = this.repository
			.createQueryBuilder('produto')
			.innerJoinAndSelect(
				subQuery,
				'mycount',
				'produto.id = mycount.produtoId',
			)
			.orderBy('produto_totalAcessos', 'DESC');

		return paginate<Produto>(queryBuilder, { limit, page });
	}

	async saveOne(input: any): Promise<Produto> {
		return this.repository.save(input as Produto);
	}

	async findOne(id: number) {
		return this.repository.findOne(id);
	}

	async saveImagem(imagens: ProdutoImagem[]) {
		return this.repositoryImagem.save(imagens);
	}

	async updateOne(id: number, produto: Produto) {
		return this.repository.update(id, produto);
	}
}
