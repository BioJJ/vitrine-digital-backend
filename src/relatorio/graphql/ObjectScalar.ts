import { Scalar, CustomScalar } from '@nestjs/graphql';
import { Kind, ValueNode } from 'graphql';

@Scalar('Object', (type) => Object)
export class ObjectScalar implements CustomScalar<Object, Object> {
  description = 'Date custom scalar type';

  parseValue(value: Object): Object {
    return value; // value from the client
  }

  serialize(value: Object): Object {
    return value; // value sent to the client
  }

  parseLiteral(ast: ValueNode): Object {
    if (ast.kind === Kind.OBJECT) {
      return ast;
    }
    return null;
  }
}