import { Module } from '@nestjs/common';
import { ProdutoModule } from 'src/produto/produto.module';
import { RelatorioResolver } from './graphql/RelatorioResolver';

@Module({
    imports: [ProdutoModule],
    providers: [RelatorioResolver]
})
export class RelatorioModule {}
