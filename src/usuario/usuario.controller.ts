import {
	Controller,
	Put,
	UseInterceptors,
	Body,
	UploadedFiles,
	Param,
	Res,
	HttpStatus,
	Get,
	Query,
} from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { UsuarioService } from './usuario.service';
import { FilesService } from 'src/files/service/files.service';
import { Response } from 'express';
import { Usuario } from 'src/entities/usuario.entity';
import { QueryOptions } from 'src/model/queryOptions.dto';
import { SearchUsuario } from 'src/model/dto/search/searchUsuario.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { removePagination } from 'src/_helpers/utils';
import { UsuarioDtoInput } from 'src/model/dto/dtoInput/usuario.dto';

@ApiTags('Usuário')
@Controller('usuarios')
export class UsuarioController {
	constructor(
		private service: UsuarioService,
		private fileService: FilesService,
	) {}

	@Get()
	@ApiOperation({ description: 'Retorna uma listagem de itens' })
	getAll(
		@Query() options: QueryOptions,
		@Query() search: SearchUsuario,
	): Promise<Pagination<Usuario>> {
		const page = this.service.getAll(
			{ ...options, route: 'teste' },
			removePagination(search),
		);

		return page.then(res => {
			res.items.forEach(item => {
				delete item.senha;
			});
			return res;
		}); // TODO: Trocar pela string de rota certa.
	}


	@Put(':id')
	@ApiConsumes('multipart/form-data')
	@UseInterceptors(
		FileFieldsInterceptor([
			{
				name: 'file',
				maxCount: 1,
			},
			{
				name: 'data',
				maxCount: 1,
			},
		]),
	)
	@ApiBody({type: UsuarioDtoInput })
	async update(
		@Body() input: any,
		@UploadedFiles() files: any,
		@Param('id') id: number,
		@Res() res: Response,
	) {
		let usuario = await this.service.findOne(id);
		let fileId = '';

		if (!usuario) {
			return res
				.status(HttpStatus.NOT_FOUND)
				.json({ message: 'Usuário não encontrado' });
		}

        const usuarioInput = JSON.parse(input.data) as Usuario;

		if (files && files.file && files.file.length > 0) {
			this.fileService.deleteFile(usuario.foto);
			fileId = `${files.file[0].id}`;
			usuarioInput.foto = fileId;
		}

		if (!usuarioInput.foto) {
            this.fileService.deleteFile(usuario.foto);
        }
        
		return res.status(HttpStatus.OK).json(await this.service.updateOne(usuarioInput))
	}
}
