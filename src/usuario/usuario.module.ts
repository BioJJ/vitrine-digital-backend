import { Module, Global } from '@nestjs/common';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from 'src/entities/usuario.entity';
import { MulterModule } from '@nestjs/platform-express';
import { GridFsMulterConfigService } from 'src/files/GridFsConfiguration.service';
import { FilesModule } from 'src/files/files.module';

// https://stackoverflow.com/questions/52862644/inject-service-into-guard-in-nest-js
// Esta anotação @Global foi adicionada para que seja possível a injeção de dependência na RoleGuard
@Global()
@Module({
	imports: [
		TypeOrmModule.forFeature([Usuario]),
		MulterModule.registerAsync({
			useClass: GridFsMulterConfigService,
    }),
    FilesModule
	],
	controllers: [UsuarioController],
	providers: [UsuarioService],
	exports: [UsuarioService],
})
export class UsuarioModule {}
