import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Usuario } from 'src/entities/usuario.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { SearchUsuario } from 'src/model/dto/search/searchUsuario.dto';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';
import { _Like } from 'src/_helpers/utils';

@Injectable()
export class UsuarioService {
	saltRounds: number = 5;

	constructor(
		@InjectRepository(Usuario) private repository: Repository<Usuario>,
	) {}

	async getUsuarioByEmail(email: string): Promise<Usuario> {
		return this.repository.findOne({ email });
	}

	async getUsuarioById(id: number): Promise<Usuario> {
		return this.repository.findOne({ id });
	}

	async createUsuario(usuario: Usuario): Promise<Usuario> {
		usuario.senha = await this.getHash(usuario.senha);
		return this.repository.save(usuario);
	}

	async getHash(password: string | undefined): Promise<string> {
		return bcrypt.hash(password, this.saltRounds);
	}

	async compareHash(
		password: string | undefined,
		hash: string | undefined,
	): Promise<boolean> {
		return bcrypt.compare(password, hash);
	}

	findOne(id: number){
		return this.repository.findOneOrFail(id);
	}

	updateOne(usuario: Usuario){
		return this.repository.save(usuario);
	}

	async getAll(options: IPaginationOptions, search: Partial<SearchUsuario>): Promise<Pagination<Usuario>> {
		const queryBuilder = this.repository.createQueryBuilder("usuario")
		.leftJoinAndSelect("usuario.grupoDePesquisa", "grupoDePesquisa")
		.leftJoinAndSelect("grupoDePesquisa.linhaDePesquisa", "linhaDePesquisa")
		.leftJoinAndSelect("usuario.orientador", "orientador")
		.leftJoinAndSelect("usuario.coorientador", "coorientador")
		if(Object.entries(search).length !== 0){
			console.log(search)
			queryBuilder.where(
				{
					nome: _Like(search.nome)
				}
			)
			search.docente ? queryBuilder.andWhere("usuario.isDocente = :docente", {docente: Boolean(search.docente)}) : null;
			search.discente ? queryBuilder.andWhere("usuario.isDiscente = :discente", {discente: Boolean(search.discente)}) : null;
			search.externo ? queryBuilder.andWhere("usuario.isExterno = :externo", {externo: Boolean(search.externo)}) : null;
		}
		return paginate<Usuario>(queryBuilder, options);
	}
}
